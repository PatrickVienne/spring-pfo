package com.teachu.example.controller;

import com.teachu.example.model.Employee;
import com.teachu.example.repository.EmployeeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class WebController {
    private final EmployeeRepository repository;

    WebController(EmployeeRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/index")
    public String index(Model model) {
        List<Employee> employees = repository.findAll();
        model.addAttribute("employees", employees);
        return "index";
    }
}
